#include <iostream>
#include <stdlib.h>

using namespace std;

void NaborTochek(double* x, double* y, int num) {
    srand(time(NULL));
    for (int i = 0; i < num; i++) {
        x[i] = rand() % 20;
        x[i] = x[i] / 10 - 1;
        y[i] = rand() % 10;
        y[i] = y[i] / 10;
    }
}

bool Popadanie(double x, double y) {
    // ��������� � ������������
    if (x >= -0.6 && x <= 0.6) {
        if (y >= 0 && y <= 0.3) {
            return true;
        }
    }

    if (x >= -1 && x <= -0.6){
            if (y >= 0 && y <= 0.3){
                // ��������
                double diagonal = (0.0 - 0.3) / (-0.6 - (-1.0));
                double intercept = 0.3 - diagonal * (-1.0);

                // ������ ���� ���� ��������� 
                if (y >= diagonal * x + intercept){
                    return true;
                }
            }
        }


    if (x >= 0.6 && x <= 1)
    {
        if (y >= 0 && y <= 0.3)
        {
            // ���������
            double diagonal = (0.0 - 0.3) / (0.6 - 1.0);
            double intercept = 0.3 - diagonal * 1.0;

            // ���� ���������
            if (y >= diagonal * x + intercept)
            {
                return true;
            }
        }
    }

    // ������������� �������
if (x >= -0.4 && x <= 0.4) {
    if (y >= 0.3 && y <= 0.6) {
            return true;
    }
}

if (x >= -0.7 && x <= -0.4) {
    if (y >= 0.3 && y <= 0.6) {
        // ���������
        double diagonal = (0.3 - 0.6) / (-0.4 - (-0.7));
        double intercept = 0.6 - diagonal * -0.7;

        // ���� ��������� 
        if (y >= diagonal * x + intercept) {
            return true;
        }
    }
}

if (x >= 0.4 && x <= 0.7)
{
    if (y >= 0.3 && y <= 0.6)
    {
        // ���������
        double diagonal = (0.3 - 0.6) / (0.4 - 0.7);
        double intercept = 0.6 - diagonal * 0.7;

        // ���� ��������� 
        if (y >= diagonal * x + intercept)
        {
            return true;
        }
    }
}

    // ��������
    if ((x - 0) * (x - 0) + (y - 0.6) * (y - 0.6) <= 0.4 * 0.4 && y >= 0.6) {
        return true;
    }

    return false;
}

int main()
{
    double count = 0.0;
    int num;
    cout << "" << endl;
    cin >> num;
    double* x_cord = new double[num];
    double* y_cord = new double[num];
    NaborTochek(x_cord, y_cord, num);
    for (int i = 0; i < num; i++) {
        if (Popadanie(x_cord[i], y_cord[i])) {
            cout << " Shot with coordinate (" << x_cord[i] << ";" << y_cord[i] << ") hit the target " << endl;
            count++;
        }
        else {
            cout << " Shot with coordinate (" << x_cord[i] << ";" << y_cord[i] << ") missed the target " << endl;
        }
    }
    cout << (count / num) * 100 << "%" << endl;
}